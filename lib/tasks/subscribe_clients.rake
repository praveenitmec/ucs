namespace :task do
  desc "First Time Hydra Subscription of clients"
  task :subscribe_clients => :environment do 
    puts "Hydra Subscription of clients is started"
     config  = YAML.load(File.open("config/clients.yml") )
     RubyHydra::RubyRedisClient.subscription_channels(config.values)
    puts "Hydra Subscription of clients is done"
  end
  
end
