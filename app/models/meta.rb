require "status_codes"
class Meta
  extend ActiveModel::Naming
  include ActiveModel::Conversion

 
  attr_accessor :status
  attr_accessor :message

 def self.get_meta_object(code)
  return  {:status=>code, :message=>StatusCode.get_status_message(code)}
 end

end