class UcsApiModel
   include UCSConstant
   attr_accessor :ucs_data, :header, :cssFiles, :items
   
   #For Header data Building
   #http://globalnav.services.com/getcontent?resource=css,js,header,footer&site=rdcv7&pagename=LP&params=EnableGeo,EnableReg,HideLogo,SmallLogo
  def self.get_header_data(params, channel_hydra_data)
    @ucs_data= UcsApiModel.new
    template_details = is_json(hydra_data[channel])
    if template_details.blank?
        @ucs_data.cssFiles = ""
        @ucs_data.header= ""
        return @ucs_data
    end
    @ucs_data.header= ""
    return @ucs_data
  end
 
 
  #this one
  def self.get_footer_data(content)
    footer_data=is_json(DATA['RDC::Production::footer']['footer-links-social']) if content == "footer_link_social"
    footer_data=is_json(DATA['RDC::Production::footer']['footer-links-network']) if content == "footer_link_network"
    footer_data=is_json(DATA['RDC::Production::footer']['footer-link-primary']) if content == "footer_link_primary"
    footer_data=is_json(DATA['RDC::Production::footer']['footer-link-secondary']) if content == "footer_link_secondary"
    footer_data=is_json(DATA['RDC::Production::footer']['footer-popover-content']) if content == "footer_popover_content"
    return get_footer_items(footer_data)
    
  end
  
   #this one
 def self.get_footer_items(footer_data)
    @items = UcsApiModel.new
   (0..footer_data.length-1).each do |i|
      if !footer_data[i]['item'].blank?
         item_data=footer_data[i]['item']
         item=Item.new
         item.item_url=item_data['item-url'] if !item_data['item-url'].blank? or item_data['item-url']!="nil"
         item.item_omtag=item_data['item-omtag'] if !item_data['item-omtag'].blank? or item_data['item-omtag']!="nil"
         item.item_icon=item_data['item-icon'] if !item_data['item-icon'].blank? or item_data['item-icon']!="nil"
         item.item_id=item_data['item-id'] if !item_data['item-id'].blank? or item_data['item-id']!="nil"
         item.item_label=item_data['item-label'] if !item_data['item-label'].blank? or item_data['item-label']!="nil"
         footer_item.items.push(item)
      end
    end
      return footer_item
 end
 
  def self.get_the_value(data)
   value={}
     data.each do |d| 
         d=d.to_hash
         d.each{|k,v|
           value[k]=v
         }
     end
     return value
  end
  
end