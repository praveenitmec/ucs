class ErrorMessage

  extend ActiveModel::Naming
  include ActiveModel::Conversion

  attr_accessor :message
  attr_accessor :code

end