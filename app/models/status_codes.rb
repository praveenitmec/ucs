class StatusCode
attr_accessor :message

def self.get_status_message(status_code)
  case status_code
    when 200
      @message = 'OK'
    when 301
      @message = 'Moved Permanently'
    when 302
      @message = 'Found'
    when 303
      @message = 'See Other'
    when 304
      @message = 'Not Modified'
    when 307
      @message = 'Temporary Redirect'
    when 308
      @message = 'Permanent Redirect'
    when 400
      @message = 'Bad Request' 
    when 401
      @message = 'Unauthorized' 
    when 403
      @message = 'Forbidden' 
    when 404
      @message = 'Not Found' 
    when 405
      @message = 'Method Not Allowed' 
    when 406
      @message = 'Not Acceptable' 
    when 407
      @message = 'Proxy Authentication Required' 
    when 408
      @message = 'Request Timeout' 
    when 409
      @message = 'Conflict'
    when 500
      @message = 'Internal Server Error'
    when 501
      @message = 'Not Implemented'
    when 502
      @message = 'Bad Gateway'
    when 503
      @message = 'Service Unavailable'
    when 504
      @message = 'Gateway Timeout'
    when 511
      @message = 'Network Authentication Required'
    when 598
      @message = 'Network read timeout error'
    when 599
      @message = 'Network connect timeout error'
  end
   return @message
end

end