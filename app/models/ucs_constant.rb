module UCSConstant
 #Here we can define all constants related to project 
  
  #Hydra Data API to fetch data from redis server 
  DEV_HYDRA_API  =  "http://localhost:4000/api/hydra_api/"
  TEST_HYDRA_API  =  "http://localhost:4000/api/hydra_api/"
  PRODUCT_HYDRA_API  =  "http://localhost:4000/api/hydra_api/"
  
  DEFAULT_SITE = "rdc"
  
  # To get The xml Data Conventions
  #From UCS.xml to get template 
  UCS_HEADER_CONFIG_GROUP = "header-navbar"
 
  DEV_ENV_CONSTANT = "Dev"
  TEST_ENV_CONSTANT = "Test"
  PRODUCTION_ENV_CONSTANT = "Production"
  
  
  DEV_BASE_CSS_JS_URL = 'http://qam.static.move.com/'
  TEST_BASE_CSS_JS_URL = 'http://qam.static.move.com/'
  PRODUCTION_BASE_CSS_JS_UR = 'http://qam.static.move.com/'
  
  SECURE_DEV_BASE_CSS_JS_URL = 'https://qam.static.move.com/'
  SECURE_TEST_BASE_CSS_JS_URL = 'https://qam.static.move.com/'
  SECURE_PRODUCTION_BASE_CSS_JS_UR = 'https://qam.static.move.com/'
 
end