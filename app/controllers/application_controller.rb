require "ucs_constant"
class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  # To get the JSON data
  def is_json(value)
    begin
      !!JSON.parse(value)
      arr=JSON.parse(value) 
      return arr
    rescue
      return value
    end
  end
 
end
