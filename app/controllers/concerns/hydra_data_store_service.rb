require "redis"
require "hydra_worker"
require "ucs_constant"
module HydraDataStoreService
  extend ActiveSupport::Concern
  included do
     attr_reader :hydra_data
     before_filter :get_hydra_data
  end
  
  def get_hydra_data
      config  = YAML.load(File.open("config/clients.yml") )
      channels = config.values
      redis_hash = Hash.new
      channels.each do |ch|
            channel= ch.split("::")
            channel=channel[0]+"/"+channel[1]+"/"+channel[2] 
            response=HTTParty.get("#{UCSConstant::DEV_HYDRA_API}"+"#{channel}") 
            next if response['data'].blank?
            redis_hash["#{ch}"]= response['data']["#{ch}"]
      end
        @hydra_data = redis_hash
  end
  
  def get_channel_name(node,environment,config_group)
    return "#{node}::#{environment}::#{config_group}"
  end
  
  def get_key_value(channel,key)
    @hydra_data =is_json(DATA["#{channel}"]["#{key}"])
  end
  
  def is_json(value)
    begin
      !!JSON.parse(value)
      arr=JSON.parse(value) 
      return arr
    rescue
      return value
    end
  end
  
  # TODO with gem to get data
  def get_redis_data
      Thread.new do
       $redis = Redis.new(:host => 'localhost', :port => 6379,:timeout => 0)
       config=YAML.load_file('config/clients.yml')
       redis_hash={}
       channels = config.values
        if redis_hash=={}
          channels.each do |ch|
            ch1=ch.split("::")
            ch1=ch1[0]+"/"+ch1[1]+"/"+ch1[2]
            response=HTTParty.get("http://localhost:3000/api/hydra_api/#{ch1}")
            response['data']["#{ch}"]
            redis_hash["#{ch}"]= response['data']["#{ch}"]
          end
       end
       $redis.subscribe(channels) do |on|
         on.message do |channel, msg|
          data = JSON.parse(msg)
          redis_hash["#{channel}"] = data["#{channel}"]
        end
       end
       return redis_hash
     end
end

end