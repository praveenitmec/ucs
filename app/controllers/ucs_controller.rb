require "status_codes"
class UcsController < ApplicationController
  protect_from_forgery with: :null_session
 
  # This is common API for calling the global nav
   #http://globalnav.services.com/getcontent?resource=css,js,header,footer&site=rdcv7&pagename=LP&params=EnableGeo,EnableReg,HideLogo,SmallLogo
  def getglobalnavContent
    if params['resource'].blank? || params['site'].blank?
      api_error =  Meta.get_meta_object(400)     
      return  render :json => {:meta=>api_error, :data=>"No Data Available"}   
    end
     resources_array  = params["resource"].split(",")
     global_nav_channel  = get_channel_name
     # global_nav_content = HYDRA_DATA[global_nav_channel]
    # if global_nav_content.blank?
       # return render :json => {:meta=> Meta.get_meta_object(200), :data=>"No Data Available" }  
    # end
     response_hash = Hash.new
     resources_array.each{|resource|
       case resource
       when "css"
         css_data  = get_data("css")
         response_hash[:css] =css_data
       when  "js"
         js_data  = get_data("js")
         response_hash[:js] =js_data
       when  "header"
         header_data = get_header_html_string()
         response_hash[:header] =header_data
       when "footer"
          footer_data  = get_footer_data
          response_hash[:footer] =footer_data
       end     
     
     }
     render :json => {:meta=> Meta.get_meta_object(200), :data => response_hash }  
end


private
  
  def get_channel_name
    siteName = params['site'].blank? ? "#{UCSConstant::DEFAULT_SITE}" :params['site'].to_s
    pageName=params['pagename'].blank? ? "":params['pagename'].to_s
    environment = UCSConstant::DEV_ENV_CONSTANT  if Rails.env.development?
    environment = UCSConstant::TEST_ENV_CONSTANT  if Rails.env.test?
    environment = UCSConstant::PRODUCTION_ENV_CONSTANT  if Rails.env.production?
    node= siteName.upcase
    channel="#{node}::#{environment}::#{UCSConstant::UCS_HEADER_CONFIG_GROUP}"
    return channel
  end
  
  def get_header_html_string() 
    render_to_string partial: 'ucs/header',  layout: false
  end
 
  def get_data(content)
      base_url = UCSConstant::SECURE_DEV_BASE_CSS_JS_URL  if Rails.env.development?
      base_url = UCSConstant::SECURE_TEST_BASE_CSS_JS_URL  if Rails.env.test?
      base_url = UCSConstant::SECURE_PRODUCTION_BASE_CSS_JS_UR   if Rails.env.production?
      data="#{base_url}lib/ucs/2.20.5/commonucs.css" if content=="css"
      data="#{base_url}lib/ucs/2.20.5/commonucs.js" if content=="js"
      data=data.split(",") if content=="js" ####if more than one js file will case
      render_to_string "/ucs/_commoncontent", :layout => false,:locals => {:content=>content,:data => data}
  end
 
 
  def get_footer_data
    # @footer_link_social=UcsApiModel.get_footer_data("footer_link_social")
    # @footer_link_network=UcsApiModel.get_footer_data("footer_link_network")
    # @footer_link_primary=UcsApiModel.get_footer_data("footer_link_primary")
    # @footer_link_secondary=UcsApiModel.get_footer_data("footer_link_secondary")
    # @footer_popover_content=UcsApiModel.get_footer_data("footer_popover_content")
    render_to_string "/ucs/_footer", :layout => false
  end

end

